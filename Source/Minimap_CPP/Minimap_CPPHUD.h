// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once 
#include "GameFramework/HUD.h"
#include "Engine.h"
#include "Minimap_CPPHUD.generated.h"


UCLASS()
class AMinimap_CPPHUD : public AHUD
{
	GENERATED_BODY()

public:
	AMinimap_CPPHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

	class UTexture2D* CompassBackground;
	class UTexture2D* SelfIcon;

	class UTexture2D* NPCBlueIcon;
	class UTexture2D* TreasureIcon;
	class UTexture2D* EnemySkullTex;
	class UTexture2D* EnemySkullUpTex;
	class UTexture2D* EnemySkullDownTex;

protected:

	/*The start location of our radar*/
	UPROPERTY(EditAnywhere, Category = Radar)
		FVector2D RadarStartLocation = FVector2D(0.1f, 0.1f);

	/*The radius of our radar*/
	UPROPERTY(EditAnywhere, Category = Radar)
		float RadarRadius = 100.f;

	UPROPERTY(EditAnywhere, Category = Radar)
		float DegreeStep = .10f;

	/*The pixel size of the drawable radar actors*/
	UPROPERTY(EditAnywhere, Category = Radar)
		float DrawPixelSize = 5.f;

	/*Returns the center of the radar as a 2d vector*/
	FVector2D GetRadarCenterPosition();

	/*Sphere height and radius for our raycast*/
	UPROPERTY(EditAnywhere, Category = Radar)
		float SphereHeight = 200.f;

	UPROPERTY(EditAnywhere, Category = Radar)
		float SphereRadius = 2750.f;

	/*Holds a reference to every actor we are currently drawing in our radar*/
	TArray<AActor*> RadarActors;

	/*The distance scale of the radar actors*/
	UPROPERTY(EditAnywhere, Category = Radar)
		float RadarDistanceScale = 25.f;

	/*Converts the given actors' location to local (based on our character)*/
	FVector2D ConvertWorldLocationToLocal(AActor* ActorToPlace);

	/*Draws the raycasted actors in our radar*/
	void DrawRaycastedActors();




	/*Draws the radar*/
	void DrawRadar();

	void DrawPlayerInRadar();

	void PerformRadarRaycast();

	void ConvertWorldLocationToLocal();




};









