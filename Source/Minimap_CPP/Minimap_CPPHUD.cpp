// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#include "Minimap_CPP.h"
#include "Minimap_CPPHUD.h"
#include "Engine/Canvas.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "Engine.h"


AMinimap_CPPHUD::AMinimap_CPPHUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshiarTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshiarTexObj.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> EnemySkullTexObj(TEXT("/Game/UI/Icon/EnemySkull_White"));
	EnemySkullTex = EnemySkullTexObj.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> EnemySkullTexObjUp(TEXT("/Game/UI/Icon/EnemySkullUp"));
	EnemySkullUpTex = EnemySkullTexObjUp.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> EnemySkullTexObjDown(TEXT("/Game/UI/Icon/EnemySkullDown"));
	EnemySkullDownTex = EnemySkullTexObjDown.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> CompassBackgroundObj(TEXT("/Game/UI/Icon/RadarOverlay2"));
	CompassBackground = CompassBackgroundObj.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> SelfIconObj(TEXT("/Game/UI/Icon/Self"));
	SelfIcon = SelfIconObj.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> NPCIconObj(TEXT("/Game/UI/Icon/NPC_Blue"));
	NPCBlueIcon = NPCIconObj.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TreasureObj(TEXT("/Game/UI/Icon/TreasureChest"));
	TreasureIcon = TreasureObj.Object;
	
}


void AMinimap_CPPHUD::DrawHUD()
{
	Super::DrawHUD();
	DrawRadar();
	DrawPlayerInRadar();
	PerformRadarRaycast();
	DrawRaycastedActors();

	//Empty the radar actors in case the player moves out of range,
	//by doing so, we have always a valid display in our radar
	RadarActors.Empty();



	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition((Center.X),
		(Center.Y + 20.0f));

	// draw the crosshair
	FCanvasTileItem TileItem(CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem(TileItem);
}

FVector2D AMinimap_CPPHUD::GetRadarCenterPosition()
{
	//If the canvas is valid, return the center as a 2d vector

	const FVector2D viewportSize = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());
	float adjustedRadius = viewportSize.Y * .16;
	FVector2D RadarCenter = FVector2D((adjustedRadius + 10.0), (adjustedRadius + 10));

	return (Canvas) ? RadarCenter : FVector2D(0, 0);
}

void AMinimap_CPPHUD::DrawRadar()
{
	FVector2D RadarCenter = GetRadarCenterPosition();

	const FVector2D viewportSize = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());
	float adjustedRadius = viewportSize.Y * .16;

	float mod = viewportSize.Y / 3000;


	//FString TestingString = FString::SanitizeFloat(adjustedRadius);
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, viewportSize.ToString());

	Canvas->DrawIcon(Canvas->MakeIcon(CompassBackground), 10, 10, mod);

	/*for (float i = 0; i < 360; i += DegreeStep)
	{
	//We want to draw a circle in order to represent our radar
	//In order to do so, we calculate the sin and cos of almost every degree
	//It it impossible to calculate each and every possible degree because they are infinite
	//Lower the degree step in case you need a more accurate circle representation

	//We multiply our coordinates by radar size
	//in order to draw a circle with radius equal to the one we will input through the editor

	//float fixedX = FMath::Cos(i) * adjustedRadius;
	//float fixedY = FMath::Sin(i) * adjustedRadius;

	//Actual draw
	//DrawLine(RadarCenter.X, RadarCenter.Y, RadarCenter.X + fixedX, RadarCenter.Y + fixedY, FLinearColor::Gray, 1.f);
	}*/

}



void AMinimap_CPPHUD::DrawPlayerInRadar()
{
	FVector2D RadarCenter = GetRadarCenterPosition();
	Canvas->DrawIcon(Canvas->MakeIcon(SelfIcon), RadarCenter.X, RadarCenter.Y, .25);
}


void AMinimap_CPPHUD::PerformRadarRaycast()
{
	APawn* Player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

	if (Player)
	{
		TArray<FHitResult> HitResults;
		FVector EndLocation = Player->GetActorLocation();
		EndLocation.Z += SphereHeight;

		FCollisionShape CollisionShape;
		CollisionShape.ShapeType = ECollisionShape::Sphere;
		CollisionShape.SetSphere(SphereRadius);

		//Perform a the necessary sweep for actors.
		//In case you're wondering how this works, read my raycast tutorial here: http://wp.me/p6hvtS-5F
		GetWorld()->SweepMultiByChannel(HitResults, Player->GetActorLocation(), EndLocation, FQuat::Identity, ECollisionChannel::ECC_WorldDynamic, CollisionShape);

		for (auto It : HitResults)
		{
			AActor* CurrentActor = It.GetActor();
			//In case the actor contains the word "Radar" as a tag, add it to our array
			if (CurrentActor && CurrentActor->ActorHasTag("Radar")) RadarActors.Add(CurrentActor);

			if (CurrentActor && CurrentActor->ActorHasTag("NPC")) RadarActors.Add(CurrentActor);
			if (CurrentActor && CurrentActor->ActorHasTag("Treasure")) RadarActors.Add(CurrentActor);
		}
	}
}

FVector2D AMinimap_CPPHUD::ConvertWorldLocationToLocal(AActor* ActorToPlace)
{
	APawn* Player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

	if (Player && ActorToPlace)
	{
		//Convert the world location to local, based on the transform of the player
		FVector ActorsLocal3dVector = Player->GetTransform().InverseTransformPosition(ActorToPlace->GetActorLocation());

		//Rotate the vector by 90 degrees counter-clockwise in order to have a valid rotation in our radar
		ActorsLocal3dVector = FRotator(0.f, -90.f, 0.f).RotateVector(ActorsLocal3dVector);

		//Apply the given distance scale
		ActorsLocal3dVector /= RadarDistanceScale;

		//Return a 2d vector based on the 3d vector we've created above
		return FVector2D(ActorsLocal3dVector);
	}
	return FVector2D(0, 0);
}


void AMinimap_CPPHUD::DrawRaycastedActors()
{
	FVector2D RadarCenter = GetRadarCenterPosition();

	for (auto It : RadarActors)
	{

		//Get the player character so you can find the location to compare. moving this elsewhere (making 'player' public)
		ACharacter* myCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
		//myCharacter->GetActorLocation();
		FVector2D convertedLocation = ConvertWorldLocationToLocal(It);

		//We want to clamp the location of our actors in order to make sure
		//that we display them inside our radar

		//To do so, I've created the following temporary vector in order to access
		//the GetClampedToMaxSize2d function. This functions returns a clamped vector (if needed)
		//to match our max length
		FVector tempVector = FVector(convertedLocation.X, convertedLocation.Y, 0.f);

		//Subtract the pixel size in order to make the radar display more accurate
		tempVector = tempVector.GetClampedToMaxSize2D(RadarRadius - DrawPixelSize);

		//Assign the converted X and Y values to the vector we want to display
		convertedLocation.X = tempVector.X;
		convertedLocation.Y = tempVector.Y;

		float DifferenceZ = myCharacter->GetActorLocation().Z - It->GetActorLocation().Z;



		//Draw Enemies
		if (It->ActorHasTag("Radar")) {

			//Target is below player
			if (DifferenceZ > 200) {
				Canvas->DrawIcon(Canvas->MakeIcon(EnemySkullDownTex), RadarCenter.X + convertedLocation.X, RadarCenter.Y + convertedLocation.Y, .25);
			}
			//Target is above player
			else if (DifferenceZ < -200) {
				Canvas->DrawIcon(Canvas->MakeIcon(EnemySkullUpTex), RadarCenter.X + convertedLocation.X, RadarCenter.Y + convertedLocation.Y, .25);
			}
			//Target is on the same floor as player
			else {
				Canvas->DrawIcon(Canvas->MakeIcon(EnemySkullTex), RadarCenter.X + convertedLocation.X, RadarCenter.Y + convertedLocation.Y, .25);
			}
		}
		else if (It->ActorHasTag("NPC")) {
			Canvas->DrawIcon(Canvas->MakeIcon(NPCBlueIcon), RadarCenter.X + convertedLocation.X, RadarCenter.Y + convertedLocation.Y, .25);
		}
		else if (It->ActorHasTag("Treasure")) {
			Canvas->DrawIcon(Canvas->MakeIcon(TreasureIcon), RadarCenter.X + convertedLocation.X, RadarCenter.Y + convertedLocation.Y, .25);	
		}

	}

}